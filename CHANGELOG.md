# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.26](https://gitlab.com/terrabitz/GoWebAppTemplate/compare/v0.0.25...v0.0.26) (2020-04-29)


### Features

* Replace logrus with zap ([43a83be](https://gitlab.com/terrabitz/GoWebAppTemplate/commit/43a83be1ab3d9929a3e55cef6cfa7fb82c5c1f9d))


### Bug Fixes

* Fix typo ([386c567](https://gitlab.com/terrabitz/GoWebAppTemplate/commit/386c567d28ffde037732e2671cf42beabce9eebe))

### [0.0.25](https://gitlab.com/terrabitz/GoWebAppTemplate/compare/v0.0.24...v0.0.25) (2020-04-28)


### Features

* Add additional security headers ([0471384](https://gitlab.com/terrabitz/GoWebAppTemplate/commit/0471384924f82d79aa258afd6f3abae136412761))
* Allow development-mode logging ([9c7cd7a](https://gitlab.com/terrabitz/GoWebAppTemplate/commit/9c7cd7aa982a04ee557afc92ed88970c91e86469))

### [0.0.24](https://gitlab.com/terrabitz/GoWebAppTemplate/compare/v0.0.23...v0.0.24) (2020-04-26)

### [0.0.23](https://gitlab.com/terrabitz/GoWebAppTemplate/compare/v0.0.22...v0.0.23) (2020-04-26)

### [0.0.22](https://gitlab.com/terrabitz/GoWebAppTemplate/compare/v0.0.21...v0.0.22) (2020-04-26)

### [0.0.21](https://gitlab.com/terrabitz/GoWebAppTemplate/compare/v0.0.20...v0.0.21) (2020-04-26)

### [0.0.21](https://gitlab.com/terrabitz/GoWebAppTemplate/compare/v0.0.20...v0.0.21) (2020-04-26)
