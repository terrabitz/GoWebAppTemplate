package internal

import (
	"errors"
	"net/http"
	"sync/atomic"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/terrabitz/GoWebAppTemplate/models"
)

// Error represents a handler error. It provides methods for a HTTP status
// code and embeds the built-in error interface.
type Error interface {
	error
	Status() int
}

// StatusError represents an error with an associated HTTP status code.
type StatusError struct {
	Code int
	Err  error
}

// Allows StatusError to satisfy the error interface.
func (se StatusError) Error() string {
	return se.Err.Error()
}

// Status returns our HTTP status code.
func (se StatusError) Status() int {
	return se.Code
}

type AppHandler func(http.ResponseWriter, *http.Request) error

type AppEndpoint struct {
	Handler AppHandler
	Server  *Server
}

func (a AppEndpoint) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := a.Handler(w, r); err != nil {
		switch e := err.(type) {
		case Error:
			// We can retrieve the status here and write out a specific
			// HTTP status code.
			a.Server.Logger.Infow(
				e.Error(),
				"code", e.Status(),
			)
			http.Error(w, e.Error(), e.Status())
		default:
			// Any error types we don't specifically look out for default
			// to serving a HTTP 500
			a.Server.Logger.Infow(
				"",
				"error", e.Error(),
			)
			http.Error(w, http.StatusText(http.StatusInternalServerError),
				http.StatusInternalServerError)
		}
	}
}

// HelloHandler says hello
func (s *Server) handleHello() *AppEndpoint {
	// swagger:route GET / hello
	//
	// Says hello to the world
	//
	// Produces:
	// - application/json
	// Schemes: http, https
	// Responses:
	// 	200: HelloResponse
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) error {
		return respond(w, s.Info, http.StatusOK)
	})
}

func (s *Server) handleCustomHello() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) error {
		params := httprouter.ParamsFromContext(r.Context())
		name := params.ByName("name")
		if name == "bob" {
			return StatusError{
				Code: http.StatusBadRequest,
				Err:  errors.New("saying hello to 'bob' is not supported"),
			}
		}
		response := models.Hello{
			Greeting: "hello",
			Name:     name,
		}
		return respond(w, response, http.StatusOK)
	})
}

func (s *Server) handleHealthz() *AppEndpoint {
	// Healthz is a simple ping utility to determine if this server is up
	// swagger:route GET /healthz ping
	//
	// Responds to a ping to indicate whether this server is up
	//
	// 		Produces:
	//		- application/json
	//
	//		Schemes: http, https
	//
	//		Responses:
	//			200: Ping
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) error {
		if atomic.LoadInt32(&s.IsHealthy) == 0 {
			return StatusError{
				Code: http.StatusServiceUnavailable,
			}
		}
		return respond(w, models.Ping{Status: "up"}, http.StatusOK)
	})
}
