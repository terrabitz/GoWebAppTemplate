package internal

import (
	"net/http"

	"github.com/slok/go-http-metrics/middleware"
)

type Middleware func(http.Handler) http.Handler

func (s *SubRouter) AddMiddleware(m ...Middleware) {
	s.Middlewares = append(s.Middlewares, m...)
}

func wrapMiddleware(handler http.Handler, middlewares ...Middleware) http.Handler {
	newHandler := handler
	// Apply middlewares in reverse so the most specific ones lie closest to the handler
	for i := len(middlewares) - 1; i >= 0; i-- {
		m := middlewares[i]
		newHandler = m(newHandler)
	}
	return newHandler
}

// PerRouteMiddleware adds middleware that requires information about the specific
// paths used (e.g. HTTP metric collection, which needs to know what path a given
// function is registered to).
type PerRouteMiddleware interface {
	AddMiddleware(path string, handler http.Handler) http.Handler
}

func wrapPerRouteMiddleware(path string, handler http.Handler, middlewares ...PerRouteMiddleware) http.Handler {
	newHandler := handler
	// Apply middlewares in reverse so the most specific ones lie closest to the handler
	for i := len(middlewares) - 1; i >= 0; i-- {
		m := middlewares[i]
		newHandler = m.AddMiddleware(path, newHandler)
	}
	return newHandler
}

type EndpointMetricsMiddleware struct {
	MetricsCollector middleware.Middleware
}

func (m *EndpointMetricsMiddleware) AddMiddleware(path string, handler http.Handler) http.Handler {
	return m.MetricsCollector.Handler(path, handler)
}
