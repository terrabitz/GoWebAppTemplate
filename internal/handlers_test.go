package internal

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-test/deep"
	json "github.com/json-iterator/go"
	"github.com/matryer/is"
	"gitlab.com/terrabitz/GoWebAppTemplate/models"
)

func Test_CustomHello(t *testing.T) {
	Is := is.New(t)
	expected := models.Hello{
		Greeting: "hello",
		Name:     "foo",
	}

	srv := Server{}
	srv.routes()
	req := httptest.NewRequest("GET", "/api/v1/hello/foo", nil)
	w := httptest.NewRecorder()
	srv.Router.ServeHTTP(w, req)
	response := models.Hello{}
	_ = json.Unmarshal(w.Body.Bytes(), &response)
	Is.Equal(w.Result().StatusCode, http.StatusOK)
	if diff := deep.Equal(response, expected); diff != nil {
		t.Error(diff)
	}
}
