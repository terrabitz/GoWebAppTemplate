package internal

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/markbates/pkger"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (s *Server) routes() {
	s.Router = httprouter.New()

	root := s.SubRoute("/")
	root.RouteFiles("/doc", pkger.Dir("/static/doc"))
	root.Route("GET", "/metrics", promhttp.Handler())

	v1 := root.SubRoute("/api/v1")
	v1.Route("GET", "/", s.handleHello())
	v1.Route("GET", "/healthz", s.handleHealthz())
	v1.Route("GET", "/hello/:name", s.handleCustomHello())
}

//nolint:unparam // Example only uses GETs
func (s *Server) Route(method, path string, handler http.Handler) {
	handler = wrapPerRouteMiddleware(path, handler, s.PerRouteMiddlewares...)
	s.Router.Handler(method, path, handler)
}

func (s *Server) SubRoute(path string) *SubRouter {
	if path[0] != '/' {
		panic("path must begin with '/' in path '" + path + "'")
	}

	// Strip trailing / (if present) as all added sub paths must start with a /
	if path[len(path)-1] == '/' {
		path = path[:len(path)-1]
	}
	return &SubRouter{
		Path:        path,
		Server:      s,
		Middlewares: make([]Middleware, 0),
	}
}

type SubRouter struct {
	// The full path prefix that this SubRouter is adding to
	Path string
	// The router to add routes to
	Server *Server
	// Middleware to append to all child routes. These get appended to an extra subroutes
	Middlewares []Middleware
}

func (s *SubRouter) SubRoute(path string) *SubRouter {
	if path[0] != '/' {
		panic("path must begin with '/' in path '" + path + "'")
	}

	// Strip trailing / (if present) as all added sub paths must start with a /
	if path[len(path)-1] == '/' {
		path = path[:len(path)-1]
	}
	newPath := s.Path + path
	return &SubRouter{
		Path:        newPath,
		Server:      s.Server,
		Middlewares: s.Middlewares,
	}
}

//nolint:unparam // Example only uses GETs
func (s *SubRouter) Route(method, path string, handler http.Handler) {
	if path[0] != '/' {
		panic("path must begin with '/' in path '" + path + "'")
	}

	fullPath := s.Path + path
	h := wrapMiddleware(handler, s.Middlewares...)

	s.Server.Route(method, fullPath, h)
}

func (s *SubRouter) RouteFiles(path string, root http.FileSystem) {
	const filePathSuffix = "/*filepath"

	if path[0] != '/' {
		panic("path must begin with '/' in path '" + path + "'")
	}
	// Strip trailing / (if present)
	if path[len(path)-1] == '/' {
		path = path[:len(path)-1]
	}

	if len(path) < 10 || path[len(path)-len(filePathSuffix):] != filePathSuffix {
		path += filePathSuffix
	}

	fileServer := http.FileServer(root)
	fileServer = wrapMiddleware(fileServer, s.Middlewares...)
	fileServer = wrapPerRouteMiddleware(path, fileServer, s.Server.PerRouteMiddlewares...)
	s.Server.Router.GET(path, func(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
		req.URL.Path = ps.ByName("filepath")
		fileServer.ServeHTTP(w, req)
	})
}
