package internal

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/julienschmidt/httprouter"
	metrics "github.com/slok/go-http-metrics/metrics/prometheus"
	"github.com/slok/go-http-metrics/middleware"
	"github.com/unrolled/secure"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// ServerInfo contains some metadata about the server. This information is
// typically baked in at compile time, so in most development cases these fields
// will be empty.
type ServerInfo struct {
	Version string
	Commit  string
	Date    string
	BuiltBy string
}

// ServerConfig contains all the configuration options for the server.
// Everything here should be operator-accessible
type ServerConfig struct {
	LogFile     string
	Port        int `valid:"range(1|65535)~'port' must be a valid port between 1-65535, required~Port must be specified"`
	Development bool
	Verbose     bool
}

// Server describes the server configuration
type Server struct {
	ServerConfig
	Info                ServerInfo
	PerRouteMiddlewares []PerRouteMiddleware
	Logger              *zap.SugaredLogger
	Router              *httprouter.Router
	MaxShutdownTime     time.Duration
	IsHealthy           int32
	httpServer          *http.Server
}

func NewServer(config ServerConfig, info ServerInfo) *Server {
	return &Server{
		ServerConfig: config,
		Info:         info,
	}
}

// Start initializes a server instance and start the server
func (s *Server) Start() {
	l, err := NewZapLogger(s.Development, s.Verbose, s.LogFile)
	if err != nil {
		panic("Could not initialize logger: " + err.Error())
	}
	defer l.Sync() //nolint:errcheck // We don't care about this error
	s.Logger = l
	s.Logger.Debug("Starting server in debug mode")
	if s.LogFile != "" {
		s.Logger.Debugw(
			"Using logfile",
			"path", s.LogFile,
		)
	}
	s.Logger.Info("Starting server")
	s.MaxShutdownTime = 30 * time.Second

	s.PerRouteMiddlewares = initPerRouteMiddleware()
	s.routes()

	r := wrapMiddleware(
		s.Router,
		initGlobalMiddleware()...,
	)

	port := s.Port
	portString := strconv.Itoa(port)
	listenAddr := ":" + portString

	s.httpServer = &http.Server{
		Addr:         listenAddr,
		Handler:      r,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-quit
		s.Shutdown()
		close(done)
	}()

	ip := getOutboundIP()
	ipPort := ip.String() + ":" + portString
	s.Logger.Info("Server is ready to handle requests at " + ipPort)
	atomic.StoreInt32(&s.IsHealthy, 1)
	if err := s.httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		s.Logger.Infow(
			"Could not listen on address",
			"address", listenAddr,
			"err", err,
		)
		return
	}

	<-done
	s.Logger.Info("Server stopped")
}

func NewZapLogger(development, verbose bool, logFile string) (*zap.SugaredLogger, error) {
	var config zap.Config
	if development {
		config = zap.NewDevelopmentConfig()
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		config.EncoderConfig.EncodeDuration = zapcore.SecondsDurationEncoder
	} else {
		config = zap.NewProductionConfig()
	}
	if logFile != "" {
		config.OutputPaths = append(config.OutputPaths, logFile)
	}
	if verbose {
		config.Level.SetLevel(zapcore.DebugLevel)
	}
	logger, err := config.Build()
	if err != nil {
		return nil, err
	}
	return logger.Sugar(), nil
}

func (s *Server) AddEndpoint(h AppHandler) *AppEndpoint {
	return &AppEndpoint{
		Server:  s,
		Handler: h,
	}
}

func (s *Server) Shutdown() {
	s.Logger.Info("Server is shutting down...")
	atomic.StoreInt32(&s.IsHealthy, 0)

	ctx, cancel := context.WithTimeout(context.Background(), s.MaxShutdownTime)
	defer cancel()

	s.httpServer.SetKeepAlivesEnabled(false)
	if err := s.httpServer.Shutdown(ctx); err != nil {
		s.Logger.Infof("Could not gracefully shutdown the server: %v\n", err)
	}
}

func initPerRouteMiddleware() []PerRouteMiddleware {
	metricsMiddleware := &EndpointMetricsMiddleware{
		MetricsCollector: middleware.New(middleware.Config{
			Recorder: metrics.NewRecorder(metrics.Config{}),
		})}

	return []PerRouteMiddleware{
		metricsMiddleware,
	}
}

func initGlobalMiddleware() []Middleware {
	featurePolicies := []string{
		"geolocation none",
		"midi none",
		"notifications none",
		"push none",
		"sync-xhr none",
		"microphone none",
		"camera none",
		"magnetometer none",
		"gyroscope none",
		"speaker self",
		"vibrate none",
		"payment none",
	}
	secureMiddleware := secure.New(secure.Options{
		FrameDeny:          true,
		BrowserXssFilter:   true,
		ContentTypeNosniff: true,
		ReferrerPolicy:     "same-origin",
		FeaturePolicy:      strings.Join(featurePolicies, ";"),
	})

	return []Middleware{
		secureMiddleware.Handler,
	}
}
