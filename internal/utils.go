package internal

import (
	"fmt"
	"log"
	"net"
	"net/http"

	json "github.com/json-iterator/go"
)

func getOutboundIP() net.IP {
	conn, err := net.Dial("udp", "1.1.1.1:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	return localAddr.IP
}

func respond(w http.ResponseWriter, data interface{}, status int) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if data != nil {
		b, err := json.Marshal(data)
		if err != nil {
			return err
		}
		fmt.Fprint(w, string(b))
	}
	return nil
}

//nolint:unused,deadcode // Example doesn't take in bodies yet
func decode(r *http.Request, v interface{}) error {
	return json.NewDecoder(r.Body).Decode(v)
}
