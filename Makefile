dev:
	gin run

build-ui:
	swagger generate spec -o api/swagger.yaml
	redoc-cli bundle -o static/doc/index.html api/swagger.yaml
	pkger

build-snapshot:
	docker run --rm --privileged -v $(shell pwd):/go/src/gitlab.com/terrabitz/GoWebAppTemplate -v /var/run/docker.sock:/var/run/docker.sock -w /go/src/gitlab.com/terrabitz/GoWebAppTemplate -e GITLAB_TOKEN goreleaser/goreleaser:v0.132.1 release --debug --snapshot --skip-publish --rm-dist

test:
	go test ./...

lint:
	golangci-lint run

tidy: 
	./build/tidy.sh

release: test lint tidy
	standard-version