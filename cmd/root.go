package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/terrabitz/GoWebAppTemplate/internal"
)

// Execute the root command
func Execute(c internal.ServerInfo) {
	var rootCmd = initRootCmd(c)
	rootCmd.Version = c.Version
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func initRootCmd(info internal.ServerInfo) *cobra.Command {
	var customConfigFile string
	cobra.OnInitialize(getInitConfig(&customConfigFile))
	cmd := &cobra.Command{
		Use:   "GoWebAppTemplate",
		Short: "GoWebAppTemplate helps do stuff",
		Long:  "GoWebAppTemplate helps do stuff",
		Run: func(cmd *cobra.Command, args []string) {
			config := internal.ServerConfig{}
			err := viper.Unmarshal(&config)
			if err != nil {
				log.Fatalf("Could not read configuration: %s", err.Error())
			}
			_, err = govalidator.ValidateStruct(config)
			if err != nil {
				log.Fatal("configuration error: " + err.Error())
			}
			s := internal.NewServer(config, info)
			s.Start()
		},
	}
	bindFlags(cmd, &customConfigFile)
	addSubcommands(cmd)
	return cmd
}

func bindFlags(cmd *cobra.Command, customConfigFile *string) {
	cmd.PersistentFlags().StringVar(customConfigFile, "config", "", "config file")

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	cmd.PersistentFlags().BoolP("verbose", "v", false, "Run with verbose logs")
	_ = viper.BindPFlag("verbose", cmd.PersistentFlags().Lookup("verbose"))

	cmd.PersistentFlags().StringP("log-file", "l", "", "File to log to")
	_ = viper.BindPFlag("logFile", cmd.PersistentFlags().Lookup("log-file"))

	cmd.PersistentFlags().IntP("port", "p", 8080, "Port to run server on")
	_ = viper.BindPFlag("port", cmd.PersistentFlags().Lookup("port"))

	cmd.PersistentFlags().Bool("development", false, "Enable development mode")
	_ = viper.BindPFlag("development", cmd.PersistentFlags().Lookup("development"))
}

func addSubcommands(cmd *cobra.Command) {
	cmd.AddCommand(newCmdGenCompletion(cmd))
	cmd.AddCommand(newCmdGenDocs(cmd))
}

// getInitConfig creates a small closure which binds a potential custom configuration file
// Since we don't know at the start whether this is needed
func getInitConfig(customConfigFile *string) func() {
	return func() {
		// Don't forget to read config either from cfgFile or from home directory!
		if (*customConfigFile) != "" {
			// Use config file from the flag.
			viper.SetConfigFile(*customConfigFile)
			if err := viper.ReadInConfig(); err != nil {
				log.Printf("Can't read custom config '%s': %e", *customConfigFile, err)
				os.Exit(1)
			}
		} else {
			viper.SetConfigName("gowebapptemplate")               // name of config file (without extension)
			viper.AddConfigPath(".")                              // optionally look for config in the working directory
			viper.AddConfigPath("$HOME/.config/gowebapptemplate") // call multiple times to add many search paths
			viper.AddConfigPath("/etc/gowebapptemplate/")         // path to look for the config file in
			_ = viper.ReadInConfig()                              // Find and read the config file
		}
	}
}
