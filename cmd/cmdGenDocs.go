package cmd

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

func newCmdGenDocs(rootCmd *cobra.Command) *cobra.Command {
	var docsValidArgs = []string{"rest", "man", "md", "yaml"}

	return &cobra.Command{
		Use:       "docs",
		Short:     "Generate docs for this package",
		Long:      fmt.Sprintf("Generate docs for this package. Must be one of the following: [%s]", strings.Join(docsValidArgs, ", ")),
		Args:      cobra.OnlyValidArgs,
		ValidArgs: docsValidArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				_ = cmd.Help()
				fmt.Printf("\n\nMust specify one of: [%s]", strings.Join(cmd.ValidArgs, ", "))
				return
			}
			switch args[0] {
			case "man":
				name := rootCmd.Name()
				header := &doc.GenManHeader{
					Title:   name,
					Section: "1",
				}
				err := doc.GenManTree(rootCmd, header, "./")
				if err != nil {
					log.Fatal(err)
				}
			case "rest":
				err := doc.GenReSTTree(rootCmd, "./")
				if err != nil {
					log.Fatal(err)
				}
			case "md":
				err := doc.GenMarkdownTree(rootCmd, "./")
				if err != nil {
					log.Fatal(err)
				}
			case "yaml":
				err := doc.GenYamlTree(rootCmd, "./")
				if err != nil {
					log.Fatal(err)
				}
			}
		},
	}
}
