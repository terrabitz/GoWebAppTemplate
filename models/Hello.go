package models

// Hello says hello to the user, with a particular greeting
//
// swagger:model
type Hello struct {
	// The greeting
	Greeting string `json:"greeting"`

	// The name of the user
	Name string `json:"name"`
}

// HelloResponse is the response containing a greeting
//
// swagger:response
type HelloResponse struct {
	// The greeting
	// in: body
	Body Hello
}
