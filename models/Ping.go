package models

// Ping provides the ping response from Healthz
//
// swagger:model
type Ping struct {
	// The server status
	//
	// example: up
	Status string `json:"status"`
}

// PingResponse responds to a ping
//
// swagger:response
type PingResponse struct {
	// The body
	// in: body
	Body Ping
}
