// Package Go web app template
//
// The purpose of this application is to provide an opinionated base structure for other APIs
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http, https
//     Host: localhost
//     BasePath: /api/v1
//     Version: 0.0.1
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: Trevor Taubitz<terrabitz@hackandsla.sh> https://hackandsla.sh
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - api_key:
//
//     SecurityDefinitions:
//     api_key:
//          type: apiKey
//          name: KEY
//          in: header
//     oauth2:
//         type: oauth2
//         authorizationUrl: /oauth2/auth
//         tokenUrl: /oauth2/token
//         in: header
//         scopes:
//           bar: foo
//         flow: accessCode
//
// swagger:meta
package main

import (
	"gitlab.com/terrabitz/GoWebAppTemplate/cmd"
	"gitlab.com/terrabitz/GoWebAppTemplate/internal"
)

//nolint:gochecknoglobals // These variables may be dynamically inserted at build-time
var (
	version = ""
	commit  = ""
	date    = ""
	builtBy = ""
)

func main() {
	cmd.Execute(internal.ServerInfo{
		Version: version,
		Commit:  commit,
		Date:    date,
		BuiltBy: builtBy,
	})
}
