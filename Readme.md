# GoWebAppTemplate

{{ description }}

## Installation

To get a pre-compiled binary for your architecture, just head over to the [releases](https://gitlab.com/terrabitz/GoWebAppTemplate/-/releases) page.

If you would like to compile it yourself:

```bash
go get gitlab.com/terrabitz/GoWebAppTemplate
```

## Usage

{{ usage }}

## Docker usage

To run the tool with Docker, just run the latest version of the `terrabitz/GoWebAppTemplate` image:

```bash
docker run \
    terrabitz/GoWebAppTemplate
```

## Help docs

## Meta

Trevor Taubitz – [@terrabitz](https://twitter.com/terrabitz)

Distributed under the MIT license. See `LICENSE` for more information.

[https://gitlab.com/terrabitz/](https://gitlab.com/terrabitz)
