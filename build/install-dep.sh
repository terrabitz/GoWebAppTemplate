#!/bin/bash

# This script is to install all required binaries into the goreleaser container
# during build

# Install Alpine APKs
apk add --update nodejs nodejs-npm make gcc jq

# Install golang binaries. Note, we do this in a temp directory to avoid adding
# these packages to the go.mod file automatically
pushd /tmp
go get github.com/go-swagger/go-swagger/cmd/swagger
go get github.com/markbates/pkger/cmd/pkger
popd

# Install NPM scripts
npm config set user 0
npm config set unsafe-perm true
npm install -g redoc-cli